# Create the database tables needed by isoonline.  Please note that
# this removes the database if it already exists.

drop database if exists isoonline;
create database isoonline;
use isoonline;

create table base (
	dir_id int64 auto_increment	primary key not null,
	dir_name varchar(255)		not null,
	active bool			not null,
	last_scanned datetime		null,
	first_scanned datetime		null,

	UNIQUE(dir_name),
	INDEX(active, last_scanned)
);

create table file (
	file_id int64 auto_increment	primary key not null,
	filename varchar(255)		not null,
	dir_id int64			not null references base(dir_id),
	mtime datetime			not null,
	size int64			not null,
	md5sum  char(32)		not null,
	sha1sum char(40)		not null,
	
	# When was the on-line file last verified?
	verified datetime		not null,

	# Set if the on-line file is missing or modified.
	broken bool			not null,

	INDEX(filename),
	UNIQUE(filename, size, mtime, dir_id),
	INDEX(md5sum),
	INDEX(size)
);

create table media_batch (
	batch_id int32 auto_increment	primary key not null,
	purchase_date datetime		not null,

	# Capacity in number of blocks.
	capacity int32			not null,

	# 2048 for CD-R and CD-RW.
	blocksize int16			not null,

	# True for CD-R, false for CD-RW.
	permanent bool			not null,
	
	speed int16			not null,	

	manufacturer varchar(255)	not null,
	label varchar(255)		not null,

	INDEX(permanent)
);
	
	
create table media (
	media_id int32 auto_increment	primary key not null,
	batch_id int32		     not null references media_batch(batch_id),
	written datetime		not null,
	verified datetime		null,
	broken bool			not null,

	INDEX(written),
	INDEX(verified)
);

create table contents (
	media int32			not null references media(media_id),
	file int64			not null references file(file_id),

	INDEX(file),
	INDEX(media)
);

insert into base (dir_name, active) values ("/movies/olymp/hotsync", 1);
insert into base (dir_name, active) values ("/movies/dc/hotsync", 1);

insert into media_batch (purchase_date, capacity, blocksize, permanent, speed,
	manufacturer, label)

values ('2001-04-21', 336225, 2048, 1, 12,
	'Traxdata', 'Traxdata silver 12X Speed 74'),

       ('2001-04-26', 335100, 2048, 0, 4,
	'Arita', 'Arita CD-RW 74H'),

       ('2002-03-15', 337275, 2048, 1, 0,
	'Biltema', 'Biltema 74 23-555'),

       ('2001-12-24', 335925, 2048, 0, 4,
	'TDK', 'TDK CD-RW650'),

       ('2003-04-14', 336075, 2048, 0, 24,
	'Plextor', 'Plextor CD-RW 74'),

       ('2003-04-14', 359847, 2048, 1, 48,
	'Plextor', 'Plextor CD-R 80 48x multispeed'),

       ('2004-01-06', 359848, 2048, 1, 48,
	'Memorex', 'Memorex CD-R 700MB 48x')
;
